#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "fake_os.h"

FakeOS os;

typedef struct {
  int quantum;
} SchedRRArgs;

void schedRR(FakeOS* os, void* args_, int a){
  SchedRRArgs* args=(SchedRRArgs*)args_;

  // look for the first process in ready
  // if none, return
  if (! os->ready.first)
    return;
  //ESTENSIONE TETRA-CORE
  // look at the first event
  // if duration>quantum
  // push front in the list of event a CPU event of duration quantum
  // alter the duration of the old event subtracting quantum
  if(a==1){
	FakePCB* pcb1=(FakePCB*) List_popFront(&os->ready);
	os->running_processore1=pcb1;
	assert(pcb1->events.first);
	ProcessEvent* e = (ProcessEvent*)pcb1->events.first;
	assert(e->type==CPU);
	if (e->duration>args->quantum) {
		ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
		qe->list.prev=qe->list.next=0;
		qe->type=CPU;
		qe->duration=args->quantum;
		e->duration-=args->quantum;
		List_pushFront(&pcb1->events, (ListItem*)qe);
	}
  }
  if(a==2){
	FakePCB* pcb2=(FakePCB*) List_popFront(&os->ready);
	os->running_processore2=pcb2;
	assert(pcb2->events.first);
	ProcessEvent* e2 = (ProcessEvent*)pcb2->events.first;
	assert(e2->type==CPU);
	if (e2->duration>args->quantum) {
		ProcessEvent* qe2=(ProcessEvent*)malloc(sizeof(ProcessEvent));
		qe2->list.prev=qe2->list.next=0;
		qe2->type=CPU;
		qe2->duration=args->quantum;
		e2->duration-=args->quantum;
		List_pushFront(&pcb2->events, (ListItem*)qe2);
	}
  }
  if(a==3){
	FakePCB* pcb3=(FakePCB*) List_popFront(&os->ready);
	os->running_processore3=pcb3;
	assert(pcb3->events.first);
	ProcessEvent* e3 = (ProcessEvent*)pcb3->events.first;
	assert(e3->type==CPU);
	if (e3->duration>args->quantum) {
		ProcessEvent* qe3=(ProcessEvent*)malloc(sizeof(ProcessEvent));
		qe3->list.prev=qe3->list.next=0;
		qe3->type=CPU;
		qe3->duration=args->quantum;
		e3->duration-=args->quantum;
		List_pushFront(&pcb3->events, (ListItem*)qe3);
	}
  }
  if(a==4){
	FakePCB* pcb4=(FakePCB*) List_popFront(&os->ready);
	os->running_processore4=pcb4;
	assert(pcb4->events.first);
	ProcessEvent* e4 = (ProcessEvent*)pcb4->events.first;
	assert(e4->type==CPU);
	if (e4->duration>args->quantum) {
		ProcessEvent* qe4=(ProcessEvent*)malloc(sizeof(ProcessEvent));
		qe4->list.prev=qe4->list.next=0;
		qe4->type=CPU;
		qe4->duration=args->quantum;
		e4->duration-=args->quantum;
		List_pushFront(&pcb4->events, (ListItem*)qe4);
	}
  }
};

int main(int argc, char** argv) {
  FakeOS_init(&os);
  SchedRRArgs srr_args;
  if (argv[1]==NULL){//
	  printf("Inserire quanto e processi\n");//
	  return 0;//
  }//
  if((int)argv[1][0]>57||(int)argv[1][0]<48){
	  printf("Inserisci quanto\n");
	  return 0;
  }
  else {
	  if (argv[1][1]!='\0') {
		if (argv[1][2]!='\0') {//
			printf("Inserire un quanto tra 1 e 15\n");//
			return 0;//
		}//
	  srr_args.quantum=(((int) argv[1][0]-48)*10+(int) argv[1][1]-48); //
	  }
	  else srr_args.quantum=(int) argv[1][0]-48; //
	  if(srr_args.quantum>15||srr_args.quantum<1){ //
		printf("Inserire un quanto tra 1 e 15\n"); //
		return 0; //
	  } //
  }//
  printf("\nQuanto registrato: %d\n\n", srr_args.quantum); //
  os.schedule_args=&srr_args;
  os.schedule_fn=schedRR;
  
  for (int i=2; i<argc; ++i){
    FakeProcess new_process;
    int num_events=FakeProcess_load(&new_process, argv[i]);
    printf("load [%s], pid: %d, events:%d \n\n",
           argv[i], new_process.pid, num_events);
    if (num_events) {
      FakeProcess* new_process_ptr=(FakeProcess*)malloc(sizeof(FakeProcess));
      *new_process_ptr=new_process;
      List_pushBack(&os.processes, (ListItem*)new_process_ptr);
    }
  }
  printf("num processes in queue %d\n", os.processes.size);
  while(os.running_processore1
		|| os.running_processore2
		|| os.running_processore3
		|| os.running_processore4
        || os.ready.first
        || os.waiting.first
        || os.processes.first){
    FakeOS_simStep(&os);
  }
}
