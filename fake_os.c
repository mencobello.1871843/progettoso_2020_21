#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "fake_os.h"

void FakeOS_init(FakeOS* os) {
  os->running_processore1=0;//
  os->running_processore2=0;//
  os->running_processore3=0;//
  os->running_processore4=0;//
  List_init(&os->ready);
  List_init(&os->waiting);
  List_init(&os->processes);
  os->timer=0;
  os->schedule_fn=0;
}

void FakeOS_createProcess(FakeOS* os, FakeProcess* p) {
  // sanity check
  assert(p->arrival_time==os->timer && "time mismatch in creation");
  // we check that in the list of PCBs there is no
  // pcb having the same pid
  assert( (!os->running_processore1 || os->running_processore1->pid!=p->pid) && "pid taken");
  assert( (!os->running_processore2 || os->running_processore2->pid!=p->pid) && "pid taken");
  assert( (!os->running_processore3 || os->running_processore3->pid!=p->pid) && "pid taken");
  assert( (!os->running_processore4 || os->running_processore4->pid!=p->pid) && "pid taken");

  ListItem* aux=os->ready.first;
  while(aux){
    FakePCB* pcb=(FakePCB*)aux;
    assert(pcb->pid!=p->pid && "pid taken");
    aux=aux->next;
  }

  aux=os->waiting.first;
  while(aux){
    FakePCB* pcb=(FakePCB*)aux;
    assert(pcb->pid!=p->pid && "pid taken");
    aux=aux->next;
  }

  // all fine, no such pcb exists
  FakePCB* new_pcb=(FakePCB*) malloc(sizeof(FakePCB));
  new_pcb->list.next=new_pcb->list.prev=0;
  new_pcb->pid=p->pid;
  new_pcb->events=p->events;

  assert(new_pcb->events.first && "process without events");

  // depending on the type of the first event
  // we put the process either in ready or in waiting
  ProcessEvent* e=(ProcessEvent*)new_pcb->events.first;
  switch(e->type){
  case CPU:
    List_pushBack(&os->ready, (ListItem*) new_pcb);
    break;
  case IO:
    List_pushBack(&os->waiting, (ListItem*) new_pcb);
    break;
  default:
    assert(0 && "illegal resource");
    ;
  }
}




void FakeOS_simStep(FakeOS* os){
  
  printf("************** TIME: %08d **************\n\n", os->timer);
  
    
  //scan process waiting to be started
  //and create all processes starting now
  ListItem* aux=os->processes.first;
  int a=1;
  while (aux){
    FakeProcess* proc=(FakeProcess*)aux;
    FakeProcess* new_process=0;
    if (proc->arrival_time==os->timer){
      new_process=proc;
    }
    aux=aux->next;
    if (new_process) {
	  if(a==1) printf("\tPROCESSES CREATION\n");
	  a++;
      printf("\tcreate pid:%d\n\n", new_process->pid);
      new_process=(FakeProcess*)List_detach(&os->processes, (ListItem*)new_process);
      FakeOS_createProcess(os, new_process);
      free(new_process);
    }
  }
  aux=os->ready.first;
  if (aux) printf("\tREADY PROCESSES\n");
  while(aux) {
	FakeProcess* p=(FakeProcess*)aux;
    aux=aux->next;
    printf("\tready pid: %d\n\n", p->pid);
  }



  // scan waiting list, and put in ready all items whose event terminates
  aux=os->waiting.first;
  if(aux) printf("\tWAITING PROCESSES\n");
  while(aux) {
    FakePCB* pcb=(FakePCB*)aux;
    aux=aux->next;
    ProcessEvent* e=(ProcessEvent*) pcb->events.first;
    printf("\twaiting pid: %d\n", pcb->pid);
    assert(e->type==IO);
    e->duration--;
    printf("\t\tremaining time:%d\n",e->duration);
    if (e->duration==0){
      printf("\t\tend burst\n");
      List_popFront(&pcb->events);
      free(e);
      List_detach(&os->waiting, (ListItem*)pcb);
      if (! pcb->events.first) {
        // kill process
        printf("\t\tend process\n\n");
        free(pcb);
      } else {
        //handle next event
        e=(ProcessEvent*) pcb->events.first;
        switch (e->type){
        case CPU:
          printf("\t\tmove to ready\n\n");
          List_pushBack(&os->ready, (ListItem*) pcb);
          break;
        case IO:
          printf("\t\tmove to waiting\n");
          List_pushBack(&os->waiting, (ListItem*) pcb);
          break;
        }
      }
    } else printf("\n");
  }

  //RUNNING DEI QUATTRO PROCESSORI
  printf("\tRUNNING PROCESSES\n");
  // decrement the duration of running
  // if event over, destroy event
  // and reschedule process
  // if last event, destroy running
  printf("\n\tPROCESSORE 1\n");
  FakePCB* running1=os->running_processore1;
  printf("\trunning pid: %d\n", running1?running1->pid:-1);
  if (running1) {
    ProcessEvent* e=(ProcessEvent*) running1->events.first;
    assert(e->type==CPU);
    e->duration--;
    printf("\t\tremaining time:%d\n",e->duration);
    if (e->duration==0){
      printf("\t\tend burst\n");
      List_popFront(&running1->events);
      free(e);
      if (! running1->events.first) {
        printf("\t\tend process\n");
        free(running1); // kill process
      } else {
        e=(ProcessEvent*) running1->events.first;
        switch (e->type){
        case CPU:
          printf("\t\tmove to ready\n");
          List_pushBack(&os->ready, (ListItem*) running1);
          break;
        case IO:
          printf("\t\tmove to waiting\n");
          List_pushBack(&os->waiting, (ListItem*) running1);
          break;
        }
      }
      os->running_processore1 = 0;
    }
  }
  
  printf("\n\tPROCESSORE 2\n");
  FakePCB* running2=os->running_processore2;
  printf("\trunning pid: %d\n", running2?running2->pid:-1);
  if (running2) {
    ProcessEvent* e=(ProcessEvent*) running2->events.first;
    assert(e->type==CPU);
    e->duration--;
    printf("\t\tremaining time:%d\n",e->duration);
    if (e->duration==0){
      printf("\t\tend burst\n");
      List_popFront(&running2->events);
      free(e);
      if (! running2->events.first) {
        printf("\t\tend process\n");
        free(running2); // kill process
      } else {
        e=(ProcessEvent*) running2->events.first;
        switch (e->type){
        case CPU:
          printf("\t\tmove to ready\n");
          List_pushBack(&os->ready, (ListItem*) running2);
          break;
        case IO:
          printf("\t\tmove to waiting\n");
          List_pushBack(&os->waiting, (ListItem*) running2);
          break;
        }
      }
      os->running_processore2 = 0;
    }
  }
  
  printf("\n\tPROCESSORE 3\n");
  FakePCB* running3=os->running_processore3;
  printf("\trunning pid: %d\n", running3?running3->pid:-1);
  if (running3) {
    ProcessEvent* e=(ProcessEvent*) running3->events.first;
    assert(e->type==CPU);
    e->duration--;
    printf("\t\tremaining time:%d\n",e->duration);
    if (e->duration==0){
      printf("\t\tend burst\n");
      List_popFront(&running3->events);
      free(e);
      if (! running3->events.first) {
        printf("\t\tend process\n");
        free(running3); // kill process
      } else {
        e=(ProcessEvent*) running3->events.first;
        switch (e->type){
        case CPU:
          printf("\t\tmove to ready\n");
          List_pushBack(&os->ready, (ListItem*) running3);
          break;
        case IO:
          printf("\t\tmove to waiting\n");
          List_pushBack(&os->waiting, (ListItem*) running3);
          break;
        }
      }
      os->running_processore3 = 0;
    }
  }
  
  printf("\n\tPROCESSORE 4\n");
  FakePCB* running4=os->running_processore4;
  printf("\trunning pid: %d\n", running4?running4->pid:-1);
  if (running4) {
    ProcessEvent* e=(ProcessEvent*) running4->events.first;
    assert(e->type==CPU);
    e->duration--;
    printf("\t\tremaining time:%d\n",e->duration);
    if (e->duration==0){
      printf("\t\tend burst\n");
      List_popFront(&running4->events);
      free(e);
      if (! running4->events.first) {
        printf("\t\tend process\n");
        free(running4); // kill process
      } else {
        e=(ProcessEvent*) running4->events.first;
        switch (e->type){
        case CPU:
          printf("\t\tmove to ready\n");
          List_pushBack(&os->ready, (ListItem*) running4);
          break;
        case IO:
          printf("\t\tmove to waiting\n");
          List_pushBack(&os->waiting, (ListItem*) running4);
          break;
        }
      }
      os->running_processore4 = 0;
    }
  }


  // call schedule, if defined
  //PER OGNI PROCESSORE
  if (os->schedule_fn && ! os->running_processore1){//
    (*os->schedule_fn)(os, os->schedule_args, 1); //
  }
  if (os->schedule_fn && ! os->running_processore2){//
    (*os->schedule_fn)(os, os->schedule_args, 2); //
  }
  if (os->schedule_fn && ! os->running_processore3){//
    (*os->schedule_fn)(os, os->schedule_args, 3); //
  }
  if (os->schedule_fn && ! os->running_processore4){//
    (*os->schedule_fn)(os, os->schedule_args, 4); //
  }

  // if running not defined and ready queue not empty
  // put the first in ready to run
  //ESTENSIONE AI QUATTRO PROCESSORI
  if (! os->running_processore1 && os->ready.first) {//
    os->running_processore1=(FakePCB*) List_popFront(&os->ready);//
  }
  if (! os->running_processore2 && os->ready.first) {//
    os->running_processore2=(FakePCB*) List_popFront(&os->ready);//
  }
  if (! os->running_processore3 && os->ready.first) {//
    os->running_processore3=(FakePCB*) List_popFront(&os->ready);//
  }
  if (! os->running_processore4 && os->ready.first) {//
    os->running_processore4=(FakePCB*) List_popFront(&os->ready);//
  }

  ++os->timer;

}

void FakeOS_destroy(FakeOS* os) {
}
